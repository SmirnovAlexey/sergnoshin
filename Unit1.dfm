object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = #1058#1077#1089#1090#1086#1074#1086#1077' '#1079#1072#1076#1072#1085#1080#1077' '#1057#1086#1074#1082#1086#1084#1073#1072#1085#1082
  ClientHeight = 575
  ClientWidth = 814
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 48
    Top = 8
    Width = 697
    Height = 233
    TabOrder = 0
    object Label1: TLabel
      Left = 72
      Top = 56
      Width = 42
      Height = 13
      Caption = #1055#1086#1088#1086#1076#1072':'
    end
    object Label2: TLabel
      Left = 72
      Top = 88
      Width = 41
      Height = 13
      Caption = #1050#1083#1080#1095#1082#1072':'
    end
    object Label3: TLabel
      Left = 72
      Top = 153
      Width = 94
      Height = 13
      Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1091#1087#1083#1077#1085#1080#1103
    end
    object Label4: TLabel
      Left = 192
      Top = 16
      Width = 215
      Height = 16
      Alignment = taCenter
      Caption = #1055#1086#1089#1090#1091#1087#1083#1077#1085#1080#1077' '#1085#1086#1074#1086#1075#1086' '#1078#1080#1074#1086#1090#1085#1086#1075#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 72
      Top = 120
      Width = 64
      Height = 13
      Caption = #1055#1086#1083#1085#1099#1093' '#1083#1077#1090':'
    end
    object Edit1: TEdit
      Left = 172
      Top = 58
      Width = 269
      Height = 21
      TabOrder = 0
    end
    object Edit2: TEdit
      Left = 172
      Top = 85
      Width = 269
      Height = 21
      TabOrder = 1
    end
    object Button1: TButton
      Left = 488
      Top = 51
      Width = 153
      Height = 25
      Caption = #1044#1072#1073#1072#1074#1080#1090#1100
      TabOrder = 2
      OnClick = Button1Click
    end
    object DateTimePicker1: TDateTimePicker
      Left = 172
      Top = 153
      Width = 269
      Height = 21
      Date = 43142.735585960650000000
      Time = 43142.735585960650000000
      TabOrder = 3
    end
    object SpinEdit1: TSpinEdit
      Left = 172
      Top = 117
      Width = 269
      Height = 22
      MaxValue = 100
      MinValue = 0
      TabOrder = 4
      Value = 0
    end
    object Button3: TButton
      Left = 488
      Top = 100
      Width = 153
      Height = 25
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      TabOrder = 5
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 488
      Top = 148
      Width = 153
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100
      TabOrder = 6
      OnClick = Button4Click
    end
  end
  object Button2: TButton
    Left = 96
    Top = 249
    Width = 249
    Height = 25
    Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1074#1089#1077#1093' '#1078#1080#1074#1086#1090#1085#1099#1093
    TabOrder = 1
    OnClick = Button2Click
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 290
    Width = 814
    Height = 285
    Align = alBottom
    DataSource = DataSource1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
  end
  object Button5: TButton
    Left = 440
    Top = 249
    Width = 249
    Height = 25
    Caption = #1055#1077#1095#1072#1090#1100' '#1086#1090#1095#1077#1090#1072
    TabOrder = 3
    OnClick = Button5Click
  end
  object DataSource1: TDataSource
    DataSet = ClientDataSet1
    Left = 776
    Top = 24
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProvider1'
    Left = 776
    Top = 120
  end
  object DataSetProvider1: TDataSetProvider
    DataSet = FDM.FDQuery1
    Left = 775
    Top = 72
  end
end
