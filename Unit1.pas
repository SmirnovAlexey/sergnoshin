unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, MidasLib,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Data.DB, DBAccess, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.Samples.Spin,
  MemDS, Vcl.Grids, Vcl.DBGrids, Datasnap.DBClient, Datasnap.Provider,
  Vcl.OleCtnrs, Unit3, Unit5, SynPdf;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Panel1: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    DateTimePicker1: TDateTimePicker;
    SpinEdit1: TSpinEdit;
    Label5: TLabel;
    Button2: TButton;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    ClientDataSet1: TClientDataSet;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    DataSetProvider1: TDataSetProvider;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
    selected_line: string;
    procedure update_grid();
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  FDM.FDTransaction1.StartTransaction; // ��������� ����������
  try
    FDM.FDCommand1.CommandText.Clear;
    FDM.FDCommand1.CommandText.Insert(0,
      'INSERT INTO admission (breed, name, age, receipt_date,transaction_date) VALUES (:breed, :name, :age, :receipt_date, :transaction_date)');
    FDM.FDCommand1.ParamByName('breed').AsString := Edit1.Text;
    FDM.FDCommand1.ParamByName('name').AsString := Edit2.Text;
    FDM.FDCommand1.ParamByName('age').AsInteger := SpinEdit1.Value;
    FDM.FDCommand1.ParamByName('receipt_date').AsDate := DateTimePicker1.Date;
    FDM.FDCommand1.ParamByName('transaction_date').AsDateTime := Now;
    FDM.FDCommand1.Execute;
    FDM.FDTransaction1.Commit;
    ShowMessage('������ ���������')
  except
    FDM.FDTransaction1.Rollback;
    ShowMessage('������ �� ���������')
  end;
  update_grid;
end;

procedure TForm1.Button3Click(Sender: TObject);

begin
  if selected_line <> '' then
  begin
    FDM.FDTransaction1.StartTransaction; // ��������� ����������
    try
      FDM.FDCommand1.CommandText.Clear;
      FDM.FDCommand1.CommandText.Insert(0,
        'UPDATE admission SET breed =:breed, name =:name, age=:age, receipt_date=:receipt_date,transaction_date = :transaction_date WHERE ID = :ID;');
      FDM.FDCommand1.ParamByName('breed').AsString := Edit1.Text;
      FDM.FDCommand1.ParamByName('name').AsString := Edit2.Text;
      FDM.FDCommand1.ParamByName('age').AsInteger := SpinEdit1.Value;
      FDM.FDCommand1.ParamByName('receipt_date').AsDate := DateTimePicker1.Date;
      FDM.FDCommand1.ParamByName('transaction_date').AsDateTime := Now;
      FDM.FDCommand1.ParamByName('id').AsInteger := StrToInt(selected_line);
      FDM.FDCommand1.Execute;
      FDM.FDTransaction1.Commit;
      ShowMessage('������ ���������')
    except
      FDM.FDTransaction1.Rollback; // ���������� ���������
      ShowMessage('������ �� ���������');
    end;
  end;
  update_grid;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  if selected_line <> '' then
  begin
    FDM.FDTransaction1.StartTransaction;
    try
      FDM.FDCommand1.CommandText.Clear;
      FDM.FDCommand1.CommandText.Insert(0,
        'DELETE FROM admission WHERE ID = :ID;');
      FDM.FDCommand1.ParamByName('id').AsInteger := StrToInt(selected_line);
      FDM.FDCommand1.Execute;
      FDM.FDTransaction1.Commit;
      ShowMessage('������ �������')
    except
      FDM.FDTransaction1.Rollback; // ���������� ���������
      ShowMessage('������ �� �������');
    end;
  end;
  update_grid;
end;

procedure TForm1.Button5Click(Sender: TObject);

var
  lPdf: TPdfDocument;
  select_text: string;

begin
  { ���������� � PDF
    if selected_line <> '' then
    begin
    lPdf := TPdfDocument.Create; // ������� ��������
    try
    select_text := 'SELECT * FROM admission where id = ' + selected_line;
    FDM.FDQuery1.Open(select_text);
    lPdf.Info.Author := 'SNoshin'; // ������ ������
    lPdf.Info.CreationDate := Now; // ������ ���� �������� ���������
    lPdf.DefaultPaperSize := psA4; // ������� ������ ������� (�4)
    lPdf.AddPage; // �������� �������� � ��������
    lPdf.Canvas.SetFont('Helvetica', 12.0, []); // ������� ��������� ������
    lPdf.Canvas.BeginText;
    try
    lPdf.Canvas.TextOut(120, 820, '����������');
    lPdf.Canvas.TextOut(20, 804, '� ����� ' + FDM.FDQuery1.fields[4]
    .AsString + ' ��������� ��������� ��������');
    lPdf.Canvas.TextOut(20, 788, '������: ' + FDM.FDQuery1.fields[1]
    .AsString);
    lPdf.Canvas.TextOut(20, 772, '�� ������: ' + FDM.FDQuery1.fields[2]
    .AsString);
    lPdf.Canvas.TextOut(20, 756, '�������: ' + FDM.FDQuery1.fields[3]
    .AsString + ' ���');
    finally
    lPdf.Canvas.EndText;
    end;
    // ��������� ���������� ����
    lPdf.SaveToFile('c:\temp\test.pdf');
    finally
    lPdf.Free;
    end;
    end else ShowMessage('�������� ��������');
    end; }

  begin
    if selected_line <> '' then
    begin
      select_text := 'SELECT * FROM admission where id = ' + selected_line;
      FDM.FDQuery1.Open(select_text);

      Form3.Visible := True;
      Form3.Memo1.Clear;
      Form3.Memo1.Lines.Add('                       ����������');
      Form3.Memo1.Lines.Add('� ����� ' + FDM.FDQuery1.fields[4].AsString +
        ' ��������� ��������� ��������');
      Form3.Memo1.Lines.Add('������: ' + FDM.FDQuery1.fields[1].AsString);
      Form3.Memo1.Lines.Add('�� ������: ' + FDM.FDQuery1.fields[2].AsString);
      Form3.Memo1.Lines.Add('�������: ' + FDM.FDQuery1.fields[3].AsString);
      FDM.FDQuery1.Close;
    end
    else
      ShowMessage('�������� ��������');
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  update_grid;
end;

procedure TForm1.update_grid();
begin
  ClientDataSet1.Active := False;
  FDM.FDQuery1.SQL.Text := 'SELECT * FROM admission';
  ClientDataSet1.Active := True;
  DBGrid1.Columns[1].Title.Caption := '������';
  DBGrid1.Columns[2].Title.Caption := '������';
  DBGrid1.Columns[3].Title.Caption := '�������';
  DBGrid1.Columns[4].Title.Caption := '���� �����������';
  DBGrid1.Columns[5].Visible := False;
  FDM.FDQuery1.Close;
end;

procedure TForm1.DBGrid1CellClick(Column: TColumn);
begin
  selected_line := DBGrid1.fields[0].AsString;
  Edit1.Text := DBGrid1.fields[1].AsString;
  Edit2.Text := DBGrid1.fields[2].AsString;
  SpinEdit1.Value := DBGrid1.fields[3].AsInteger;
  DateTimePicker1.Date := DBGrid1.fields[4].AsDateTime;
end;





end.
