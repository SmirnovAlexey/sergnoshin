unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,Printers;

type
  TForm3 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    PrintDialog1: TPrintDialog;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.Button1Click(Sender: TObject);
var
  Prn : TPrinter;
  FPrn : TextFile;
  i:Integer;
  StrTmp : String;
begin
    if not PrintDialog1.Execute then Exit;
  Prn := Printer;
  AssignPrn(FPrn);

  try
    //��������� ������� ��� ������. �. �. �������� ������.
    Rewrite(FPrn);
    //������������� ��� ����� �������� ����� �� �����, �����
    //����������� � ����.
    Prn.Canvas.Font := Memo1.Font;
    prn.Canvas.MoveTo(25,30);
    //������ ����� ����.
    for i := 0 to Memo1.Lines.Count - 1 do begin
      StrTmp := Memo1.Lines[i];
      Writeln(FPrn, StrTmp);
    end;
  finally
    //��������� �������. �. �. ���������� ������.
    CloseFile(FPrn);
  end;

end;

end.
